package br.edu.especializacao.dao;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.edu.especializacao.model.Registro;

public class RegistroDao implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Inject
	EntityManager em;
	
	private DAO<Registro> dao;
	
	@PostConstruct
	void init() {
		this.dao = new DAO<Registro>(this.em, Registro.class);
	}

	public Registro buscaPorId(Integer registroId) {
		return this.dao.buscaPorId(registroId);
	}

	public void adiciona(Registro registro) {
		this.dao.adiciona(registro);
	}

	public void atualiza(Registro registro) {
		this.dao.atualiza(registro);
	}

	public void remove(Registro registro) {
		this.dao.remove(registro);
	}

	public List<Registro> listaTodos() {
		return this.dao.listaTodos();
	}

}
