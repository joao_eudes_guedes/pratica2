package br.edu.especializacao.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;


import br.edu.especializacao.dao.RegistroDao;
import br.edu.especializacao.model.Registro;
import br.edu.especializacao.tx.Transacional;

@Named
@ViewScoped
public class RegistroBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Registro registro = new Registro();

	private Integer id;
	
	private List<Registro> registros;

	@Inject
	private RegistroDao dao;

	public Integer getRegistroId() {
		return id;
	}

	public void setRegistroId(Integer id) {
		this.id = id;
	}

	public void carregarRegistroPelaId() {
		this.registro = this.dao.buscaPorId(id);
	}

	@Transacional
	public String gravar() {
		System.out.println("Gravando registro " + this.registro.getNome());

		if (this.registro.getId() == null) {
			this.dao.adiciona(this.registro);
		} else {
			this.dao.atualiza(this.registro);
		}

		this.registro = new Registro();

		return "registro?faces-redirect=true";
//		return "livro?faces-redirect=true";
	}

	@Transacional
	public void remover(Registro registro) {
		System.out.println("Removendo registro " + registro.getNome());
		this.dao.remove(registro);
	}

	

	public Registro getRegistro() {
		return registro;
	}

	public void setRegistro(Registro registro) {
		this.registro = registro;
	}
	
	public List<Registro> getRegistros() {

		if (this.registros == null) {
			this.registros = this.dao.listaTodos();
		}
		return this.registros;
	}
	
}
