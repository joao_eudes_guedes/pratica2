package br.edu.especializacao.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Registro implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer id;
	
	
	private String nome;
	private String email;
	private String tipocontato;
	private String comentario;

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	@Override
	public String toString() {
		return "Registro [id=" + id + ", nome=" + nome + ", email=" + email + ", comentario=" + comentario + "]";
	}

	public String getTipocontato() {
		return tipocontato;
	}

	public void setTipocontato(String tipocontato) {
		this.tipocontato = tipocontato;
	}

	

}
